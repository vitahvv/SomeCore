﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCore.Repo
{
    public interface IRepository<T>  where T : class , new()
    {
        IEnumerable<T> Get();
        IEnumerable<T> GetAll();
        void Add(T item);
        T Find(int id);
        void Update(T item);
        void Remove(int id);
    }

}
