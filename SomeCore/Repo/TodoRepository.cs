﻿using SomeCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCore.Repo
{
    class TodoRepository : IRepository<Todo>
    {
        private List<Todo> repo;

        public TodoRepository()
        {
            repo = new List<Todo>
         {
             new Todo{ Id= 1, Title = "Wasser", Completed = true },
             new Todo{ Id= 2, Title = "Brot", Completed = true },
             new Todo{ Id= 3, Title = "Kase", Completed = false},
             new Todo{ Id= 4, Title = "Butter", Completed = false}
         };
        }

        public IEnumerable<Todo> Get()
        {
            return repo.ToList();
        }

        public IEnumerable<Todo> GetAll() {

            return repo.Where(i =>i.Completed = true).ToList();
        }
        public void Add(Todo item)
        {
            repo.Add(item);
        }
        public Todo Find(int id) {

            return repo.FirstOrDefault(i => i.Id == id);
        }
        public void Update(Todo item) {

            repo.Insert(item.Id, item);
        }
        public void Remove(int id) {

            repo.RemoveAt(id);

        }    
    }
}
