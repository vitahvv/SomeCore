﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCore.Repo
{
    class ProductRepository : IRepository<Product> 
    {
        private  List<Product> repo;

        public ProductRepository()
        {
            repo = new List<Product>
         {
             new Product{ Id= 1, NameProduct= "Wasser",ValueProduct = 8},
             new Product{ Id= 2, NameProduct= "Brot",ValueProduct = 15},
             new Product{ Id= 3, NameProduct= "Kase",ValueProduct = 55},
             new Product{ Id= 1, NameProduct= "Butter",ValueProduct = 45}
         };            
        }

        public IEnumerable<Product> Get() { return repo; }
        public IEnumerable<Product> GetAll() { return repo; }
        public void Add(Product product)
        {
        }
        public Product Find(int id) { return null; }
        public void Update(Product product) { }
        public void Remove(int id) { }


    }
}
