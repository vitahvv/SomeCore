﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SomeCore.Model;
using SomeCore.Repo;

namespace SomeCore
{
    public class Startup
    {

      
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
           // Configuration = configuration;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json",
                 optional: true,
                    reloadOnChange: true)
                .AddJsonFile($"appsettings.{ env.EnvironmentName}.json",   optional: true)   
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        //public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddSingleton<IRepository<Todo>, TodoRepository>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this meth, od to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello world");
            //}
            //);
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")
              ),
                RequestPath = "/" + "node_modules"
            });

            app.UseMvc(routes =>
            {
              routes.MapRoute(
                    name: "default",
                    template: " { controller = Home}/{ action = Index}/{id?}");   
            });

            app.UseMvc();
        }
    }
}
