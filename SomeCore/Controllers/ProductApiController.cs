﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SomeCore.Repo;

namespace SomeCore.Controllers
{
    [Route("api/[controller]")]
    public class ProductApiController : Controller
    {
        private readonly IRepository<Product> _productRepository;
        public ProductApiController(IRepository<Product> repository)
        {
            _productRepository = repository;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var productsFromRepo = _productRepository.GetAll();
            if (productsFromRepo == null)
                return NotFound();           
            return Ok(productsFromRepo);
           
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var productFromRepo = _productRepository.Find(id);
            if (productFromRepo == null)
                return NotFound();            
            return Ok(productFromRepo);           
        }

        [HttpPost]
        public IActionResult Post([FromBody]Product product)
        {
            if (product == null)
                return BadRequest();
            //	return	HTTP	response	status	code	400
            _productRepository.Add(product);
            return CreatedAtRoute("GetProduct",
            new { id = product.Id }, product);
            //	return	HTTP	response	status	code	201
        }                       [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Product product)
        {
            if (product == null || product.Id != id)
                return BadRequest();           
            var productFromRepo = _productRepository.Find(id);
            if (productFromRepo == null)
                return NotFound();
            //	return	HTTP	response	status	code	404
            _productRepository.Update(product);
            return new NoContentResult();
            //	return	HTTP	response	status	code	204
        }        [HttpPut("{id}")]
        public IActionResult Patch(int id, [FromBody]Product product)
        {
            if (product == null)
                return BadRequest();
            //	return	HTTP	response	status	code	400
            var productFromRepo = _productRepository.Find(id);
            if (productFromRepo == null)
                return NotFound();
            //	return	HTTP	response	status	code	404
            productFromRepo.Id = product.Id;
            _productRepository.Update(product);
            return new NoContentResult();
            //	return	HTTP	response	status	code	204
        }        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var productFromRepo = _productRepository.Find(id);
            if (productFromRepo == null)
                return NotFound();
            //	return	HTTP	response	status	code	404
            _productRepository.Remove(id);
            return new NoContentResult();
            //	return	HTTP	response	status	code	204
        }
    }
}
