﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SomeCore.Model;
using SomeCore.Repo;

namespace SomeCore.Controllers
{
    [Produces("application/json")]
    [Route("api/Todos")]
    public class TodosController : Controller
    {
        private  readonly IRepository<Todo> _todoRepositiry;
        public TodosController(IRepository<Todo> repository)
        {
            _todoRepositiry = repository;
        }

        [HttpGet]     
        public IEnumerable<Todo> Get()
        {
            return _todoRepositiry.Get();
        }

        [HttpGet]
        [Route("pending-only")]
        public IEnumerable<Todo> GetPendingOnly()
        {
            return _todoRepositiry.GetAll();
        }

        // POST api/todos 
        [HttpPost]
        public Todo Post([FromBody]Todo item)
        {
            _todoRepositiry.Add(item);
            return item;
        }

        // PUT api/todos/id 
        [HttpPut("{id}")]
        public Todo Put(int id, [FromBody]Todo item)
        {            
            _todoRepositiry.Update(item);
            return item;
        }


        // DELETE api/todos/id 
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _todoRepositiry.Remove(id);
        }
    }
}
