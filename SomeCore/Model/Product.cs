﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCore.Repo
{
    public class Product
    {
        public int Id { get; set; }
        public string NameProduct { get; set; }
        public int ValueProduct { get; set; }    
    }
}
